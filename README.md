This project is a very simple end-to-end test for DAST. 

The project runs a DAST scan against a WebGoat Docker image, then runs some very simple checks against the results to verify the scan ran successfully. 
